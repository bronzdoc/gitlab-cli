# gitlab-cli

## Installation

After checking out the repo, run `bundle exec rake install`

## Usage

`$ gitlab-cli`

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

