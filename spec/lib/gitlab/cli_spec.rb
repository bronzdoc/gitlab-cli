require "gitlab/cli"

RSpec.describe Gitlab::Cli do
  describe "#url_response_time" do
    before do
      @url_stub = stub_request(:get, "https://gitlab.com").
        with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'Host'=>'gitlab.com', 'User-Agent'=>'Ruby'}).
        to_return(:status => 200, :body => "", :headers => {})
    end

    after do
      WebMock.reset!
    end

    it "should return a url response time" do
      response_time = Gitlab::Cli.url_response_time("https://gitlab.com")

      expect(@url_stub).to have_been_requested
      expect(response_time.is_a?(Numeric)).to be(true)
    end
  end
end
