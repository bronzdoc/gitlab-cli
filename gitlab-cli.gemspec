# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'gitlab/cli/version'

Gem::Specification.new do |spec|
  spec.name          = "gitlab-cli"
  spec.version       = Gitlab::Cli::VERSION
  spec.authors       = ["bronzdoc"]
  spec.email         = ["lsagastume1990@gmail.com"]

  spec.summary       = %q{"Check gitlab response time"}
  spec.description   = %q{"Check gitlab response time"}
  spec.homepage      = "https://gitlab.com/bronzdoc/gitlab-response-checker"
  spec.license       = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.13"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.6"
  spec.add_development_dependency "webmock", "~> 1.24"
end
