require "gitlab/cli/version"
require "net/https"

module Gitlab
  module Cli
    def self.url_response_time(url)
      uri = URI(url)
      start_time = Time.now
      Net::HTTP.get(uri)
      end_time = Time.now

      time_diff_milli(start_time, end_time)
    end

    def self.time_diff_milli(start, finish)
      ((finish - start) * 1000.0).round(2)
    end
  end
end
